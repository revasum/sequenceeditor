﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SequencerInterface.Definitions;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for OpenSequenceFile.xaml
    /// </summary>
    public partial class OpenSequenceFileView : Window
    {
        Tools.SequenceFileFinder m_SequenceFileFinder;


        public OpenSequenceFileView(Window parent, OpenSequenceFileViewModel openSequenceFileViewModel, Tools.SequenceFileFinder sequenceFileFinder)
        {
            m_SequenceFileFinder = sequenceFileFinder;
            OpenSequenceFileViewModel model = openSequenceFileViewModel;
            DataContext = model;
            InitializeComponent();

            // center this window against the parent
            this.Owner = parent;
            this.Topmost = true;

            if (parent.WindowState == WindowState.Normal)
            {
                this.Left = (parent.Left + (parent.Width / 2)) - (this.Width / 2);
                this.Top = (parent.Top + (parent.Height / 2)) - (this.Height / 2);
            }
            else
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }

            m_SequenceFileFinder.StartParsingFileInfoFromDirectory(SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);
        }

        private OpenSequenceFileViewModel GetModelFromDataContext()
        {
            OpenSequenceFileViewModel model = null;

            if ((DataContext != null) && (DataContext is OpenSequenceFileViewModel))
            {
                model = ((OpenSequenceFileViewModel)DataContext);
            }
            else
            {
                throw new Exception("Data context is an invalid type");
            }

            return model;
        }

        private void btnSelectDirectory_Click(object sender, RoutedEventArgs e)
        {
            OpenSequenceFileViewModel model = GetModelFromDataContext();

            if( model != null)
            {
                string newSequenceFilePath = m_SequenceFileFinder.SelectSequenceFilesDirectory();

                if( newSequenceFilePath != null)
                {
                    model.CurrentDirectory = newSequenceFilePath;

                    m_SequenceFileFinder.StartParsingFileInfoFromDirectory(newSequenceFilePath);
                }
            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenSequenceFileViewModel model = GetModelFromDataContext();

            if((model != null) && (lbSequenceFileInfo.SelectedItem != null))
            {
                m_SequenceFileFinder.CurrentSelectedFile = ((SequenceFileInfo)lbSequenceFileInfo.SelectedItem).FilenameDirectoryPath;
            }

            Close();
        }

        private void lbiSequenceFileInfo_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            OpenSequenceFileViewModel model = GetModelFromDataContext();

            if ((model != null) && (lbSequenceFileInfo.SelectedItem != null))
            {
                m_SequenceFileFinder.CurrentSelectedFile = ((SequenceFileInfo)lbSequenceFileInfo.SelectedItem).FilenameDirectoryPath;
            }

            Close();
        }
    }
}
