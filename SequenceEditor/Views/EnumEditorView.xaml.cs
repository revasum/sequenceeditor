﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for EnumEditorView.xaml
    /// </summary>
    public partial class EnumEditorView : UserControl
    {
        public EnumEditorView()
        {
            this.DataContext = new EnumEditorViewModel(); 
            this.InitializeComponent();
        }
    }
}
