﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SequenceEditor.Definitions;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for ModuleView.xaml
    /// </summary>
    public partial class ModuleView : UserControl
    {
        public ModuleView(SequenceEditorMachine machine)
        {
            this.DataContext = new ModuleViewModel(machine); 
            InitializeComponent();
        }
        public ModuleView(ModuleViewModel myVm)
        {
            this.DataContext = myVm;
            InitializeComponent();
        }
    }
}
