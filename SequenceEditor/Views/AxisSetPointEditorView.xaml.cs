﻿using System.Windows.Controls;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for AxisSetPointEditorView.xaml
    /// </summary>
    public partial class AxisSetPointEditorView : UserControl
    {
        public AxisSetPointEditorView()
        {
            this.DataContext = new AxisSetPointEditorViewModel();
            this.InitializeComponent();
        }
    }
}
