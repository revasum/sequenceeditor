﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SequencerInterface.Definitions;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for SequencerStatusView.xaml
    /// </summary>
    public partial class SequencerStatusView : UserControl
    {
        public SequencerStatusView()
        {
            //DataContext = new Definitions.SequencerStatus();
            InitializeComponent();
        }

        public SequencerStatusView(SequencerStatus myVm)
        {
            DataContext = myVm;
            InitializeComponent();
        }
    }
}
