﻿using SequencerInterface.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SequenceEditor.Definitions;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for SequenceEditorView.xaml
    /// </summary>
    public partial class SequenceEditorView : UserControl
    {
        public SequenceEditorView(SequenceEditorMachine machine)
        {
            DataContext = new SequenceEditorViewModel(machine);
            InitializeComponent();

            Name = ((SequenceEditorViewModel)DataContext).MyModule.Name;
        }

        public SequenceEditorView(SequenceEditorViewModel myVm)
        {
            DataContext = myVm;
            InitializeComponent();

            Name = ((SequenceEditorViewModel)DataContext).MyModule.Name;
        }

        
    }
}
