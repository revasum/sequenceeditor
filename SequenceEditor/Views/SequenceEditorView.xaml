﻿<UserControl x:Class="SequenceEditor.SequenceEditorView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:libdef="clr-namespace:SequencerInterface.Definitions;assembly=SequencerInterface"
             xmlns:xctk="http://schemas.xceed.com/wpf/xaml/datagrid"
             xmlns:editor="clr-namespace:SequenceEditor"
             mc:Ignorable="d" 
             d:DesignHeight="800" d:DesignWidth="1400">
    
    <UserControl.Resources>
        <DataTemplate x:Key="StepTemplate" >
            <Border BorderBrush="LightGray" BorderThickness="1" Padding="4">
                <DockPanel d:DataContext="{d:DesignInstance Type=libdef:TableRecord}">
                    <StackPanel Width="150" DockPanel.Dock="Left">
                        <StackPanel Orientation="Horizontal" >
                            <TextBlock Style="{StaticResource TextBlockStyle}" Text="Step: " TextAlignment="Right" />
                            <TextBlock Style="{StaticResource TextBlockStyle}" Text="{Binding StepNumber}"/>
                        </StackPanel>
                        <StackPanel Orientation="Horizontal" >
                            <TextBlock ToolTip="Advance to next step if wait time has elapsed AND no axes or submodules are actively receiving commands.&#x0a;Enter 'NONE' to ignore wait time for this step." Style="{StaticResource TextBlockStyle}"  Text="Wait Time: " TextAlignment="Right"/>
                            <TextBox Style="{StaticResource TextBoxStyle}" Text="{Binding TimeString}"/>
                        </StackPanel>
                        <StackPanel Orientation="Horizontal" >
                            <TextBlock ToolTip="If timeout has elapsed the sequence step has failed, all motion is stopped and a timeout fault is thrown." Style="{StaticResource TextBlockStyle}"  Text="Timeout: " TextAlignment="Right" />
                            <TextBox Style="{StaticResource TextBoxStyle}" Text="{Binding WatchDogTime}" />
                        </StackPanel>
                    </StackPanel>
                    <StackPanel HorizontalAlignment="Left">
                        <TextBox MinWidth="170" Style="{StaticResource TextBoxStyle}"  Text="{Binding Comment}" HorizontalAlignment="Left" />
                        <TextBlock Text="{Binding SummaryBind}"/>
                    </StackPanel>
                    <StackPanel VerticalAlignment="Center">
                        <StackPanel Orientation="Horizontal" HorizontalAlignment="Right" DockPanel.Dock="Right">
                            <Button HorizontalAlignment="Right" Style="{StaticResource ButtonStyle}" Content="Add Before" Height="36" Width="70" ToolTip="Insert step before this step" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=ListView}, Path=DataContext.MyModule.CurrentSequenceTable.AddStepBeforeCommand}" CommandParameter="{Binding}" />
                            <Button HorizontalAlignment="Right" Style="{StaticResource ButtonStyle}" Content="Paste Before" Height="36" Width="70" ToolTip="Paste step from the clipboard before this step" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=ListView}, Path=DataContext.MyModule.CurrentSequenceTable.PasteBeforeStepCommand}" CommandParameter="{Binding}" />
                        </StackPanel>
                        <StackPanel Orientation="Horizontal" HorizontalAlignment="Right" DockPanel.Dock="Right">
                            <Button HorizontalAlignment="Right" Style="{StaticResource ButtonStyle}" Content="Add After" Height="36" Width="70" ToolTip="Insert step after this step" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=ListView}, Path=DataContext.MyModule.CurrentSequenceTable.AddStepAfterCommand}" CommandParameter="{Binding}" />
                            <Button HorizontalAlignment="Right" Style="{StaticResource ButtonStyle}" Content="Paste After" Height="36" Width="70" ToolTip="Paste step from the clipboard after this step" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=ListView}, Path=DataContext.MyModule.CurrentSequenceTable.PasteAfterStepCommand}" CommandParameter="{Binding}" />
                        </StackPanel>
                        <StackPanel Orientation="Horizontal" HorizontalAlignment="Right" DockPanel.Dock="Right">
                            <Button HorizontalAlignment="Right" Style="{StaticResource ButtonStyle}" Content="Copy" Height="36" Width="70" ToolTip="Copy this step to the clipboard" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=ListView}, Path=DataContext.MyModule.CurrentSequenceTable.CopyStepCommand}" CommandParameter="{Binding}" />
                            <Button HorizontalAlignment="Right" Style="{StaticResource ButtonStyle}" Content="Delete" Height="36" Width="70" ToolTip="Delete this step" Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=ListView}, Path=DataContext.MyModule.CurrentSequenceTable.DeleteSelectedStepCommand}" CommandParameter="{Binding}" />
                        </StackPanel>
                    </StackPanel>
                </DockPanel>
            </Border>
        </DataTemplate>
        
        <DataTemplate x:Key="EndpointTemplate" >
            <StackPanel Orientation="Horizontal"  d:DataContext="{d:DesignInstance Type=libdef:EndPointGroup}">
                <ComboBox ItemsSource="{Binding Path=Endpoints}" 
                          DisplayMemberPath="Key" SelectedValuePath="Value" 
                          SelectedValue="{Binding Path=SelectedEndPoint}" 
                          Style="{StaticResource ComboBoxStyle}" />
                
                <TextBox Style="{StaticResource TextBoxStyle}" Text="{Binding Value}"/>
            </StackPanel>
        </DataTemplate>

        <DataTemplate x:Key="CommandTemplate" >
            <StackPanel>
                <StackPanel Orientation="Horizontal"  d:DataContext="{d:DesignInstance Type=libdef:CommandGroup}">
                    <ComboBox ItemsSource="{Binding Path=Objects}" 
                          DisplayMemberPath="Key" SelectedValuePath="Value" 
                          SelectedValue="{Binding Path=SelectedObject}" 
                          Style="{StaticResource ComboBoxStyle}" />
                    
                    <ComboBox ItemsSource="{Binding Path=CommandItemSource}" 
                          DisplayMemberPath="Key" SelectedValuePath="Value" 
                          SelectedValue="{Binding Path=SelectedCommand}" 
                          Style="{StaticResource ComboBoxStyle}" />
                    
                    <TextBox Style="{StaticResource TextBoxStyleLong}" Text="{Binding SValue}"/>
                </StackPanel>
            </StackPanel>
        </DataTemplate>

        <DataTemplate x:Key="SetpointTemplate" >
            <StackPanel d:DataContext="{d:DesignInstance Type=libdef:SetPointDefinition}" Orientation="Horizontal" >
                <TextBlock Style="{StaticResource TextBlockStyle}" Text="{Binding Type}"/>
                <TextBox Style="{StaticResource TextBoxStyle}" Text="{Binding Value}" 
                         Visibility="{Binding NotUsesPredefinedSetPoints, Converter={StaticResource BooleanToVisibilityConverter}}"/>
                <ComboBox ItemsSource="{Binding Path=PredefinedSetPoints}" 
                          Visibility="{Binding UsesPredefinedSetPoints, Converter={StaticResource BooleanToVisibilityConverter}}"
                          DisplayMemberPath="Key" SelectedValuePath="Value" 
                          SelectedValue="{Binding Path=Value}" 
                          SelectedItem="-"
                          Style="{StaticResource ComboBoxStyle}" />
            </StackPanel>
        </DataTemplate>
        
        <DataTemplate x:Key="AxisTemplate" >
            <StackPanel Orientation="Horizontal" d:DataContext="{d:DesignInstance Type=libdef:Axis}">
                <TextBlock Style="{StaticResource TextBlockStyleLong}"  Text="{Binding AxisName}" FontWeight="Bold"/>
                <StackPanel>
                    <StackPanel Orientation="Horizontal">
                        <TextBlock Style="{StaticResource TextBlockStyle}" Text="Type" />
                        <ComboBox SelectedItem="{Binding Path=MotionType}" 
                                  ItemsSource="{Binding MotionTypeValues}"
                                  Style="{StaticResource ComboBoxStyle}"/>
                    </StackPanel>
                    <StackPanel Orientation="Horizontal">
                        <TextBlock Style="{StaticResource TextBlockStyle}" Text="Direction"/>
                        <ComboBox SelectedItem="{Binding Path=Direction}" 
                                  ItemsSource="{Binding MotionTableDirectionValues}"
                                  Style="{StaticResource ComboBoxStyle}"/>
                    </StackPanel>
                    <StackPanel Orientation="Horizontal">
                        <TextBlock Style="{StaticResource TextBlockStyle}" Text="BufferMode"/>
                        <ComboBox SelectedItem="{Binding Path=BufferMode}" 
                                  ItemsSource="{Binding BufferModeValues}"
                                  Style="{StaticResource ComboBoxStyle}"/>
                    </StackPanel>
                </StackPanel>
                <ListView ItemTemplate="{StaticResource SetpointTemplate}"
                          ItemsSource="{Binding Path=SetPoints}"/>
            </StackPanel>
        </DataTemplate>

        <DataTemplate x:Key="FlagTemplate" >
            <StackPanel Orientation="Horizontal"  d:DataContext="{d:DesignInstance Type=libdef:FlagGroup}">
                <ComboBox ItemsSource="{Binding Path=Flags}" 
                          DisplayMemberPath="Key" SelectedValuePath="Value" 
                          SelectedValue="{Binding Path=SelectedFlag}" 
                          Style="{StaticResource ComboBoxStyle}" />
            </StackPanel>
        </DataTemplate>
    </UserControl.Resources>
    
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="4*"/>
            <ColumnDefinition Width="4*"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="50"/>
            <RowDefinition/>
            <RowDefinition Height="50"/>
        </Grid.RowDefinitions>

        <Grid >
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="270"/>
                <ColumnDefinition Width="*"/>
            </Grid.ColumnDefinitions>
            <StackPanel Orientation="Horizontal" Grid.Column="0" Grid.Row="0" >
                <Button Style="{StaticResource ButtonStyle}" Content="New" Command="{Binding NewCommand}"/>
                <Button Style="{StaticResource ButtonStyle}" Content="Save" Command="{Binding SaveCommand}"/>
                <Button Style="{StaticResource ButtonStyle}" Content="Save As" Command="{Binding SaveAsCommand}"/>
                <Button Style="{StaticResource ButtonStyle}" Content="Open" Command="{Binding OpenCommand}"/>
            </StackPanel>
            <StackPanel Grid.Column="1" Grid.Row="0"  Width="320" Orientation="Horizontal" Margin="5,0,5,0"  HorizontalAlignment="Left" >
                <Grid >
                    <Grid.RowDefinitions>
                        <RowDefinition Height="25"/>
                        <RowDefinition Height="25"/>
                    </Grid.RowDefinitions>
                    <StackPanel Grid.Column="0" Grid.Row="0" Orientation="Horizontal" Margin="5,0,5,0"  HorizontalAlignment="Left" >
                        <TextBlock Width="70" Style="{StaticResource TextBlockStyleLong}" Text="Connected:" HorizontalAlignment="Right"/>
                        <TextBlock Style="{StaticResource TextBlockStyleLong}" Text="{Binding MyModule.Connected}" HorizontalAlignment="Left" Width="60"/>
                    </StackPanel>
                    <StackPanel Grid.Column="0" Grid.Row="1" Orientation="Horizontal" Margin="5,0,5,0"  HorizontalAlignment="Left" >
                        <TextBlock Width="70" Style="{StaticResource TextBlockStyleLong}" Text="Editing File:" HorizontalAlignment="Right"/>
                        <TextBlock Width="330" Style="{StaticResource TextBlockStyleLong}" Text="{Binding MyModule.CurrentSequenceTable.Name }" HorizontalAlignment="Left"/>
                    </StackPanel>
                </Grid>
            </StackPanel>
        </Grid>
        <Grid Grid.Column="1" Grid.Row="0">
            <Grid   >
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="80"/>
                </Grid.ColumnDefinitions>
                <StackPanel Grid.Column="0" >
                    <editor:SequencerStatusView DataContext="{Binding MyModule.LiveSequencerStatus}" />
                </StackPanel>
                <StackPanel Grid.Column="1" VerticalAlignment="Center" >
                    <Button Style="{StaticResource ButtonStyle}" Content="Close" Command="{Binding RelativeSource={RelativeSource AncestorType={x:Type editor:MainEditor}}, Path=DataContext.CloseModuleCommand}"/>
                </StackPanel>
            </Grid >
        </Grid>
        
        <StackPanel Grid.Column="1" Grid.Row="0" MinWidth="240" >
        </StackPanel>

        <ListView Grid.Column="0" Grid.Row="1"
                  x:Name="StepListView" 
                  HorizontalContentAlignment="Stretch"
                  ItemTemplate="{StaticResource StepTemplate}"
                  ItemsSource="{Binding MyModule.CurrentSequenceTable.SequenceSteps}"
                  SelectedIndex="{Binding MyModule.CurrentSequenceTable.SelectedIndex}"
                  SelectedItem="{Binding MyModule.CurrentSequenceTable.SelectedItem}">

            <ListView.ItemContainerStyle>
                <Style TargetType="ListViewItem">
                    <Style.Setters>
                        <Setter Property="Padding" Value="0" />
                    </Style.Setters>
                    <!--<Style.Triggers>
                        <Trigger Property="Button.IsPressed" Value="true">
                            <Setter Property="IsSelected" Value="true" />
                        </Trigger>
                        <Trigger Property="Button.IsFocused" Value="true">
                            <Setter Property="IsSelected" Value="true" />
                        </Trigger>
                    </Style.Triggers>-->
                </Style>
            </ListView.ItemContainerStyle>
        </ListView>
	
        <ScrollViewer Grid.Column="1" MinWidth="450" Grid.Row="1">
            <StackPanel MinWidth="450" >
                <Expander Header="Endpoints" IsExpanded="True">
                    <ListView ItemTemplate="{StaticResource EndpointTemplate}"
                         ItemsSource="{Binding MyModule.CurrentSequenceTable.SelectedItem.EndPointGroups}"
                         HorizontalContentAlignment="Stretch"
                         SelectedItem="{Binding MyModule.CurrentSequenceTable.SelectedItem.SelectedEndPoint}">
                    </ListView>
                </Expander>
                <Expander Header="Commands" IsExpanded="True">
                    <ListView ItemTemplate="{StaticResource CommandTemplate}"
                         ItemsSource="{Binding MyModule.CurrentSequenceTable.SelectedItem.CommandGroups}"
                         HorizontalContentAlignment="Stretch"
                         SelectedItem="{Binding MyModule.CurrentSequenceTable.SelectedItem.SelectedCommand}">
                    </ListView>
                </Expander>
                <Expander Header="Axes" IsExpanded="True">
                    <ListView ItemTemplate="{StaticResource AxisTemplate}"
                         ItemsSource="{Binding MyModule.CurrentSequenceTable.SelectedItem.AxisRecords}"
                         HorizontalContentAlignment="Stretch"
                         SelectedItem="{Binding MyModule.CurrentSequenceTable.SelectedItem.SelectedAxis}">
                    </ListView>
                </Expander>
                <Expander Header="Sequence Flags" IsExpanded="True">
                    <ListView ItemTemplate="{StaticResource FlagTemplate}" 
                         ItemsSource="{Binding MyModule.CurrentSequenceTable.SelectedItem.SelectedStepFlags}"
                         HorizontalContentAlignment="Stretch"
                         SelectedItem="{Binding MyModule.CurrentSequenceTable.SelectedItem.SelectedStepFlag}">
                    </ListView>
                </Expander>
            </StackPanel>
        </ScrollViewer>
        <StackPanel Orientation="Horizontal" Grid.Column="0" Grid.Row="2">
            <Button Style="{StaticResource ButtonStyle}" Content="Execute" Command="{Binding ExecuteCommand}" ToolTip="Perform (1)Connect, (2)Reset, (3)Upload, and then (4)Run; automatically" />

            <Button Style="{StaticResource ButtonStyle}" Content="Reset" Command="{Binding ResetCommand}" ToolTip="Perform Step 2: Reset; independently" />
            <Button Style="{StaticResource ButtonStyle}" Content="Upload" Command="{Binding UploadCommand}" ToolTip="Perform Step 3: Upload; independently" />
            <Button Style="{StaticResource ButtonStyle}" Content="Run" Command="{Binding RunCommand}" ToolTip="Perform Step 4: Run; independently" />
            <Button Style="{StaticResource ButtonStyle}" Content="Next Step" Command="{Binding NextStepCommand}" ToolTip="Jump to the next step in the sequence" />
            <Button Style="{StaticResource ButtonStyle}" Content="Abort" Command="{Binding AbortCommand}" ToolTip="Abort the current sequence table" />
        </StackPanel>
        
        <DockPanel Grid.Column="1" Grid.Row="2"  MaxHeight="50" >
            <TextBlock Style="{StaticResource TextBlockStyleLong}" Text="Error Messages:" HorizontalAlignment="Right" Width="90"/>
            <ScrollViewer MinWidth="360" >
                <Border BorderThickness="1" BorderBrush="Gray">
                    <TextBlock Text="{Binding MyModule.ErrorMessages}" HorizontalAlignment="Left"/>
                </Border>
            </ScrollViewer>
        </DockPanel>
        
    </Grid>
</UserControl>
