﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SequenceEditor
{
    public class SettingBindingExtension : Binding
    {
        const Int32 MinimumScreenWidth = 500;
        const Int32 MinimumScreenHeight = 500;



        public SettingBindingExtension()
        {
            Initialize(String.Empty);
        }

        public SettingBindingExtension(String path) : base(path)
        {
            Initialize(path);
        }

        private void Initialize(String path)
        {
            this.Source = (SequenceEditor.Properties.Settings)SequenceEditor.Properties.Settings.Default;


            // consider dual monitor setups, and switching back and forth between one and two screens
            Size workingScreenSize = this.GetWorkingScreenSize();


            // multiple monitor support
            if (path.Equals("Left"))
            {
                this.CheckLeftValue(workingScreenSize);
            }
            else if (path.Equals("Top"))
            {
                this.CheckTopValue(workingScreenSize);
            }
            else if (path.Equals("Width"))
            {
                this.CheckWidthValue(workingScreenSize);
            }
            else if (path.Equals("Height"))
            {
                this.CheckHeightValue(workingScreenSize);
            }

            this.Mode = BindingMode.TwoWay;
        }

        private void CheckLeftValue(Size workingScreenSize)
        {
            Int32 left = (Int32)SequenceEditor.Properties.Settings.Default.Left;
            Int32 minLeft = 0;
            Int32 maxLeft = (workingScreenSize.Width - MinimumScreenWidth);


            left = Math.Max(left, minLeft);
            left = Math.Min(left, maxLeft);

            ((SequenceEditor.Properties.Settings)this.Source).Left = left;
        }

        private void CheckTopValue(Size workingScreenSize)
        {
            Int32 top = (Int32)SequenceEditor.Properties.Settings.Default.Top;
            Int32 minTop = 0;
            Int32 maxTop = (workingScreenSize.Height - MinimumScreenHeight);


            top = Math.Max(top, minTop);
            top = Math.Min(top, maxTop);

            ((SequenceEditor.Properties.Settings)this.Source).Top = top;
        }


        private void CheckWidthValue(Size workingScreenSize)
        {
            Int32 width = (Int32)SequenceEditor.Properties.Settings.Default.Width;
            Int32 minWidth = MinimumScreenWidth;
            Int32 maxWidth = workingScreenSize.Width;


            width = Math.Max(width, minWidth);
            width = Math.Min(width, maxWidth);

            ((SequenceEditor.Properties.Settings)this.Source).Width = width;
        }

        private void CheckHeightValue(Size workingScreenSize)
        {
            Int32 height = (Int32)SequenceEditor.Properties.Settings.Default.Height;
            Int32 minHeight = MinimumScreenHeight;
            Int32 maxHeight = workingScreenSize.Height;


            height = Math.Max(height, minHeight);
            height = Math.Min(height, maxHeight);

            ((SequenceEditor.Properties.Settings)this.Source).Height = height;
        }

        private Size GetWorkingScreenSize()
        {
            Size result = new Size();


            for (int i = 0; i < System.Windows.Forms.Screen.AllScreens.Length; i++)
            {
                // since Revasum developers are the only ones ever using Sequence Editor, we don't need to worry about this?
                // we are assuming monitors are oriented side-by-side
                result.Width += System.Windows.Forms.Screen.AllScreens[i].WorkingArea.Width;

                if (i == 0)
                {
                    result.Height += System.Windows.Forms.Screen.AllScreens[i].WorkingArea.Height;
                }
            }

            return result;
        }
    }
}
