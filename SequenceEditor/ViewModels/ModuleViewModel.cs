﻿using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using SequenceEditor.Definitions;

namespace SequenceEditor 
{
    public class ModuleViewModel: INotifyPropertyChanged
    {
        private string fileName;
        private string fileDir;
        private bool unsavedChanges = false;
        private SequenceEditorMachine m_Machine = null;

        private SequenceEditorModule myModule;
        public SequenceEditorModule MyModule 
        {
            get { return myModule; }
            set
            {
                if (value == myModule) return;
                myModule = value;
                OnPropertyChanged("MyModule");
            }
        }

        public ModuleViewModel(SequenceEditorMachine machine)
        {
            m_Machine = machine;
            MyModule = new SequenceEditorModule(SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);
        }

        #region Commands
        private ICommand newModuleCommand;
        public ICommand NewModuleCommand
        {
            get
            {
                if (newModuleCommand == null)
                    newModuleCommand = new RelayCommand(param => this.newModule(), null);
                return newModuleCommand;
            }
        }
        private void newModule()
        {
            MyModule = new SequenceEditorModule(SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory); 
        }

        private ICommand openModuleCommand;
        public ICommand OpenModuleCommand
        {
            get
            {
                if (openModuleCommand == null)
                    openModuleCommand = new RelayCommand(param => this.openModule(), null);
                return openModuleCommand;
            }
        }
        private void openModule()
        {
            bool cancelled;

            Module deserializedModule = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<Module>(Defaults.DefaultSequenceEditorDirectory + @"\Modules",
                                                   ".mod",
                                                   out fileDir,
                                                   out fileName,
                                                   out cancelled);
            if (!cancelled)
            {
                if (MyModule == null)
                {
                    MyModule = new SequenceEditorModule(SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);
                }

                if (deserializedModule != null)
                {
                    MyModule.CopyDeserializedModule(deserializedModule);
                }
            }
        }

        private ICommand saveModuleCommand;
        public ICommand SaveModuleCommand
        {
            get
            {
                if (saveModuleCommand == null)
                    saveModuleCommand = new RelayCommand(param => this.saveModule(), null);
                return saveModuleCommand;
            }
        }
        private void saveModule()
        {
            // Create a Module object to serialize properly
            Module serializedModule = new Module(MyModule, SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);

            XmlSerializerTools.
                Serializer<Module>(serializedModule, fileDir,Defaults.DefaultSettings.EncryptFiles);
        }

        private ICommand saveAsModuleCommand;
        public ICommand SaveAsModuleCommand
        {
            get
            {
                if (saveAsModuleCommand == null)
                    saveAsModuleCommand = new RelayCommand(param => this.saveAsModule(), null);
                return saveAsModuleCommand;
            }
        }
        private void saveAsModule()
        {
            // Create a Module object to serialize properly
            Module serializedModule = new Module(MyModule, SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);

            Tools.SequencerXMLSerializerTools.
                SerializerWithSavePrompt<Module>(serializedModule,
                                                 Defaults.DefaultSequenceEditorDirectory + @"\Modules",
                                                 ".mod",
                                                 out fileDir,
                                                 out fileName);
        }

        private ICommand exitCommand;
        public ICommand ExitCommand
        {
            get
            {
                if (exitCommand == null)
                    exitCommand = new RelayCommand(param => this.exit(), null);
                return exitCommand;
            }
        }
        private void exit()
        {
            if (unsavedChanges)
            {
                var result = MessageBox.Show("There are unsaved changes. Would you like to save now?", myModule.Name, MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                    throw new NotImplementedException();
                else if (result == MessageBoxResult.Cancel)
                    return;
            }
            Application.Current.Shutdown();
        }

        private ICommand openEndpointsCommand;
        public ICommand OpenEndpointsCommand
        {
            get
            {
                if (openEndpointsCommand == null)
                    openEndpointsCommand = new RelayCommand(param => this.openEndpoints(), null);
                return openEndpointsCommand;
            }
        }
        private void openEndpoints()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.Endpoints = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out location,
                                                                                   out name,
                                                                                   out cancelled);

            if(!cancelled)
            {
                MyModule.EndpointsFileLocation = location;
            }
        }

        private ICommand openObjectsCommand;
        public ICommand OpenObjectsCommand
        {
            get
            {
                if (openObjectsCommand == null)
                    openObjectsCommand = new RelayCommand(param => this.openObjects(), null);
                return openObjectsCommand;
            }
        }
        private void openObjects()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.Objects = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out location,
                                                                                   out name,
                                                                                   out cancelled);
            if(!cancelled)
            {
                MyModule.ObjectsFileLocation = location;
            }
        }

        private ICommand openCommandsCommand;
        public ICommand OpenCommandsCommand
        {
            get
            {
                if (openCommandsCommand == null)
                    openCommandsCommand = new RelayCommand(param => this.openCommands(), null);
                return openCommandsCommand;
            }
        }
        private void openCommands()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.Commands = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out location,
                                                                                   out name,
                                                                                   out cancelled);
            if(!cancelled)
            {
                MyModule.CommandsFileLocation = location;
            }
        }

        private ICommand openSequenceStepFlagCommand;
        public ICommand OpenSequenceStepFlagCommand
        {
            get
            {
                if (openSequenceStepFlagCommand == null)
                    openSequenceStepFlagCommand = new RelayCommand(param => this.openSequenceStepFlags(), null);
                return openSequenceStepFlagCommand;
            }
        }
        private void openSequenceStepFlags()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.SequenceStepFlags = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out location,
                                                                                   out name,
                                                                                   out cancelled);

            if(!cancelled)
            {
                MyModule.SequenceStepFlagFileLocation = location;
            }
        }

        private ICommand openSequenceProcessFlagCommand;
        public ICommand OpenSequenceProcessFlagCommand
        {
            get
            {
                if (openSequenceProcessFlagCommand == null)
                    openSequenceProcessFlagCommand = new RelayCommand(param => this.openProcessStepFlags(), null);
                return openSequenceProcessFlagCommand;
            }
        }
        private void openProcessStepFlags()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.SequenceProcessFlags = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out location,
                                                                                   out name,
                                                                                   out cancelled);

            if(!cancelled)
            {
                MyModule.SequenceProcessFlagFileLocation = location;
            }
        }

        private ICommand openObjectCommandCascadeCommand;
        public ICommand OpenObjectCommandCascadeCommand
        {
            get
            {
                if (openObjectCommandCascadeCommand == null)
                    openObjectCommandCascadeCommand = new RelayCommand(param => this.openObjectCommandCascade(), null);
                return openObjectCommandCascadeCommand;
            }
        }

        private ICommand openAxisSetPointDefinitionsCommand;
        public ICommand OpenAxisSetPointDefinitionsCommand
        {
            get
            {
                if (openAxisSetPointDefinitionsCommand == null)
                    openAxisSetPointDefinitionsCommand = new RelayCommand(param => this.openAxisSetPointDefinitions(), null);
                return openAxisSetPointDefinitionsCommand;
            }
        }
        private void openObjectCommandCascade()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.ObjectCommandCascade = Tools.SequencerXMLSerializerTools.
                    DeserializerWithOpenPrompt<ObservableCollection<EnumKeyLocationPair>>(Defaults.DefaultSequenceEditorDirectory + @"\EnumRelationships",
                                                                                          ".rel",
                                                                                          out location,
                                                                                          out name,
                                                                                          out cancelled);
            if(!cancelled)
            {
                MyModule.ObjectCommandCascadeFileLocation = location;
            }
        }
        private void openAxisSetPointDefinitions()
        {
            string location, name;
            bool cancelled;

            MyModule.SequencerModuleDefinitions.AxisSetPointDefinitions = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<AxisDefinition>>(Defaults.DefaultSequenceEditorDirectory + @"\AxisDefinitions",
                                                                                 ".axi",
                                                                                 out location,
                                                                                 out name,
                                                                                 out cancelled);

            if(!cancelled)
            {
                MyModule.AxisSetPointDefinitionsFileLocation = location;
            }
        }


        #endregion
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
