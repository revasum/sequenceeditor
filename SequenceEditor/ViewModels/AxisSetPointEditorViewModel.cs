﻿using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace SequenceEditor
{
    class AxisSetPointEditorViewModel: INotifyPropertyChanged
    {
        private string m_FileDirectory;
        private string m_FileName;

        private ObservableCollection<AxisDefinition> axisSetPointDefinitions;
        public ObservableCollection<AxisDefinition> AxisSetPointDefinitions 
        {
            get { return axisSetPointDefinitions; } 
            set
            {
                if (value == axisSetPointDefinitions)
                    return;
                axisSetPointDefinitions = value;
                OnPropertyChanged("AxisSetPointDefinitions");
            }
        }

        public AxisSetPointEditorViewModel()
        {
            AxisSetPointDefinitions = new ObservableCollection<AxisDefinition>();
        }

        private void addAxis()
        {
            var temp = new AxisDefinition();

            temp.SetPoints.Add(new SetPointDefinition() 
                { TypeEnum = SetPointDefinition.MotionSetPointTypes.Position, PredefinedSetPoints = new ObservableCollection<EnumKeyDoublePair>() });
            temp.SetPoints.Add(new SetPointDefinition() 
                { TypeEnum = SetPointDefinition.MotionSetPointTypes.Velocity, PredefinedSetPoints = new ObservableCollection<EnumKeyDoublePair>() });
            temp.SetPoints.Add(new SetPointDefinition() 
                { TypeEnum = SetPointDefinition.MotionSetPointTypes.Acceleration, PredefinedSetPoints = new ObservableCollection<EnumKeyDoublePair>() });
            temp.SetPoints.Add(new SetPointDefinition() 
                { TypeEnum = SetPointDefinition.MotionSetPointTypes.Deceleration, PredefinedSetPoints = new ObservableCollection<EnumKeyDoublePair>() });
            temp.SetPoints.Add(new SetPointDefinition() 
                { TypeEnum = SetPointDefinition.MotionSetPointTypes.Jerk, PredefinedSetPoints = new ObservableCollection<EnumKeyDoublePair>() });

            AxisSetPointDefinitions.Add(temp);
        }
        public int SelectedAxis = 0;
        private void deleteAxis()
        {
            if (SelectedAxis >= 0 && SelectedAxis < AxisSetPointDefinitions.Count && AxisSetPointDefinitions.Count > 1)
                AxisSetPointDefinitions.RemoveAt(SelectedAxis);
        }
        private void newAxes()
        {
            AxisSetPointDefinitions = new ObservableCollection<AxisDefinition>();
            m_FileDirectory = "";
            m_FileName = "";
        }

        private void saveAxis()
        {
            XmlSerializerTools.
                Serializer<ObservableCollection<AxisDefinition>>(AxisSetPointDefinitions,
                                                                 m_FileDirectory,
                                                                 Defaults.DefaultSettings.EncryptFiles);
        }
        private void saveAsAxis()
        {
            Tools.SequencerXMLSerializerTools.
                SerializerWithSavePrompt<ObservableCollection<AxisDefinition>>(AxisSetPointDefinitions,
                                                                               Defaults.DefaultSequenceEditorDirectory + @"\AxisDefinitions",
                                                                               ".axi",
                                                                               out m_FileDirectory,
                                                                               out m_FileName);
        }

        private void openAxis()
        {
            AxisSetPointDefinitions = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<AxisDefinition>>(Defaults.DefaultSequenceEditorDirectory + @"\AxisDefinitions",
                                                                                 ".axi",
                                                                                 out m_FileDirectory,
                                                                                 out m_FileName,
                                                                                 out bool cancelled);
        }

        #region Commands
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(param => this.addAxis(), null);
                return addCommand;
            }
        }
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                    deleteCommand = new RelayCommand(param => this.deleteAxis(), null);
                return deleteCommand;
            }
        }
        private ICommand newCommand;
        public ICommand NewCommand
        {
            get
            {
                if (newCommand == null)
                    newCommand = new RelayCommand(param => this.newAxes(), null);
                return newCommand;
            }
        }
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                    saveCommand = new RelayCommand(param => this.saveAxis(), null);
                return saveCommand;
            }
        }
        private ICommand saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                if (saveAsCommand == null)
                    saveAsCommand = new RelayCommand(param => this.saveAsAxis(), null);
                return saveAsCommand;
            }
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get
            {
                if (openCommand == null)
                    openCommand = new RelayCommand(param => this.openAxis(), null);
                return openCommand;
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
