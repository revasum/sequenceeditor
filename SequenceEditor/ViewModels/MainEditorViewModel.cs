﻿using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SequenceEditor.Definitions;

namespace SequenceEditor
{
    public class MainEditorViewModel : INotifyPropertyChanged
    {
        private SequenceEditorMachine myMachine;
        public SequenceEditorMachine MyMachine 
        {
            get { return myMachine; }
            set
            {
                if (myMachine == value)
                    return;
                myMachine = value;
                OnPropertyChanged("MyMachine");
            }
        }
        private bool unsavedChanges = false; 

        private string machineName;
        public string MachineName
        {
            get { return machineName; }
            set
            {
                if (machineName == value)
                    return;
                machineName = value;
                OnPropertyChanged("MachineName");
            }
        }

        private int selectedTabIndex;
        public int SelectedTabIndex
        {
            get { return selectedTabIndex; }
            set
            {
                if (selectedTabIndex == value)
                    return;
                selectedTabIndex = value;
                OnPropertyChanged("SelectedTabIndex");

                if (value == 0)
                    NotOnHomeScreen = false;
                else
                    NotOnHomeScreen = true;
            }
        }
        private bool notOnHomeScreen;
        public bool NotOnHomeScreen
        {
            get { return notOnHomeScreen; }
            set
            {
                if (notOnHomeScreen == value) return;
                notOnHomeScreen = value;
                OnPropertyChanged("NotOnHomeScreen");
            }
        }

        private Object selectedTabItem;
        public Object SelectedTabItem
        {
            get { return selectedTabItem; }
            set
            {
                if (selectedTabItem == value)
                    return;
                selectedTabItem = value;
                OnPropertyChanged("SelectedTabItem");

                if (SelectedTabItem is SequenceEditorView)
                    SEVM_MenuBindings = (value as SequenceEditorView).DataContext as SequenceEditorViewModel;
                else
                    SEVM_MenuBindings = nullSEVM;
            }
        }
        private SequenceEditorViewModel nullSEVM = new SequenceEditorViewModel(new SequenceEditorMachine());
        private SequenceEditorViewModel sEVM_MenuBindings;
        public SequenceEditorViewModel SEVM_MenuBindings
        {
            get { return sEVM_MenuBindings;}
            set
            {
                if (sEVM_MenuBindings == value)
                    return;
                sEVM_MenuBindings = value;
                OnPropertyChanged("SEVM_MenuBindings");
            }
        }

        private SequenceEditorModule selectedModule;
        public SequenceEditorModule SelectedModule 
        {
            get { return selectedModule; }
            set
            {
                if (value == selectedModule) return;
                selectedModule = value;
                OnPropertyChanged("SelectedModule");
            }
        }

        public ObservableCollection<UserControl> TabItems { get; set; }

        public MainEditorViewModel()
        {
            TabItems = new ObservableCollection<UserControl>();

            Machine deserializedMachine = XmlSerializerTools.
                Deserializer<Machine>(Defaults.DefaultSettings.MachineConfigFile);

            if(MyMachine == null)
            {
                MyMachine = new SequenceEditorMachine();
            }

            if (deserializedMachine != null)
            {
                MyMachine.CopyDeserializedModule(deserializedMachine);
            }

            MyMachine.Init();
            MyMachine.EncryptFiles = Defaults.DefaultSettings.EncryptFiles;

            TabItems.Add(new HomeView(this));
        }
        #region Commands
        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get
            {
                try
                {
                    if (openCommand == null)
                    {
                        openCommand = new RelayCommand(param => this.openMachine(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Open Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return openCommand;
            }
        }
        private void openMachine()
        {
            string fileDir, fileName;
            bool cancelled;

            Machine loaded = Tools.SequencerXMLSerializerTools.DeserializerWithOpenPrompt<Machine>(Defaults.DefaultSequenceEditorDirectory + @"\Machines",
                                                                                                       ".mac",
                                                                                                       out fileDir,
                                                                                                       out fileName,
                                                                                                       out cancelled);

            if (!cancelled && (loaded != null))
            {
                if (MyMachine == null)
                {
                    MyMachine = new SequenceEditorMachine();
                }

                MyMachine.CopyDeserializedModule(loaded);
                MyMachine.Init();
                MyMachine.EncryptFiles = Defaults.DefaultSettings.EncryptFiles;

                Defaults.DefaultSettings.MachineConfigFile = fileDir;
                Defaults.DefaultSettings.MachineName = MyMachine.Name;
                Defaults.SaveDefaultSettings();

                while (TabItems.Count > 0)
                {
                    TabItems.RemoveAt(0);
                }

                TabItems.Add(new HomeView(this));
                SelectedTabIndex = 0;
            }
        }

        private ICommand exitCommand;
        public ICommand ExitCommand
        {
            get
            {
                try
                {
                    if (exitCommand == null)
                    {
                        exitCommand = new RelayCommand(param => this.exit(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Exit Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return exitCommand;
            }
        }

        private ICommand viewModuleCommand;
        public ICommand ViewModuleCommand
        {
            get
            {
                try
                {
                    if (viewModuleCommand == null)
                    {
                        viewModuleCommand = new RelayCommand(param => this.viewModule(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: View Module Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return viewModuleCommand;
            }
        }

        private ICommand closeModuleCommand;
        public ICommand CloseModuleCommand
        {
            get
            {
                try
                {
                    if (closeModuleCommand == null)
                    {
                        closeModuleCommand = new RelayCommand(param => this.closeModule(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Close Module Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return closeModuleCommand;
            }
        }

        private ICommand connectToAdsCommand;
        public ICommand ConnectToAdsCommand
        {
            get
            {
                try
                {
                    if (connectToAdsCommand == null)
                    {
                        connectToAdsCommand = new RelayCommand(param => this.MyMachine.ConnectToAds(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Connect To ADS Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return connectToAdsCommand;
            }
        }

        private ICommand disconnectFromAdsCommand;
        public ICommand DisconnectFromAdsCommand
        {
            get
            {
                try
                {
                    if (disconnectFromAdsCommand == null)
                    {
                        disconnectFromAdsCommand = new RelayCommand(param => this.MyMachine.DisconnectFromAds(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Disconnect From ADS Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return disconnectFromAdsCommand;
            }
        }

        private void exit()
        {
            if (unsavedChanges)
            {
                var result = MessageBox.Show("There are unsaved changes. Would you like to save now?", myMachine.Name, MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                    throw new NotImplementedException();
                else if (result == MessageBoxResult.Cancel)
                    return;
            }

            Application.Current.Shutdown();
        }

        private ICommand editEnumCommand;
        public ICommand EditEnumCommand
        {
            get
            {
                try
                {
                    if (editEnumCommand == null)
                    {
                        editEnumCommand = new RelayCommand(param => this.editEnum(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Edit Enum Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return editEnumCommand;
            }
        }

        private void editEnum()
        {
            Window editWindow = new Window();
            editWindow.Content = new EnumEditorView();
            editWindow.Title = "Enumeration Editor";
            editWindow.SizeToContent = SizeToContent.WidthAndHeight; 
            editWindow.Show(); 
        }

        private ICommand editCascadeCommand;
        public ICommand EditCascadeCommand
        {
            get
            {
                try
                {
                    if (editCascadeCommand == null)
                    {
                        editCascadeCommand = new RelayCommand(param => this.editCascade(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Edit Cascade Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return editCascadeCommand;
            }
        }
        private void editCascade()
        {
            Window editWindow = new Window();
            editWindow.Content = new CascadingEnumEditorView();
            editWindow.Title = "Cascading Enumeration Editor";
            editWindow.SizeToContent = SizeToContent.WidthAndHeight; 
            editWindow.Show();
        }

        private ICommand editAxesCommand;
        public ICommand EditAxesCommand
        {
            get
            {
                try
                {
                    if (editAxesCommand == null)
                    {
                        editAxesCommand = new RelayCommand(param => this.editAxes(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Edit Axes Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return editAxesCommand;
            }
        }

        private void editAxes()
        {
            Window editWindow = new Window();
            editWindow.Content = new AxisSetPointEditorView();
            editWindow.Title = "Axes Editor";
            editWindow.SizeToContent = SizeToContent.WidthAndHeight; 
            editWindow.Show();
        }

        private ICommand editModuleCommand;
        public ICommand EditModuleCommand
        {
            get
            {
                try
                {
                    if (editModuleCommand == null)
                    {
                        editModuleCommand = new RelayCommand(param => this.editModule(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Edit Module Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return editModuleCommand;
            }
        }

        private void editModule()
        {
            Window editWindow = new Window();
            editWindow.Content = new ModuleView(MyMachine);
            editWindow.Title = "Module Editor";
            editWindow.SizeToContent = SizeToContent.WidthAndHeight; 
            editWindow.Show();
        }

        private ICommand editMachineCommand;
        public ICommand EditMachineCommand
        {
            get
            {
                try
                {
                    if (editMachineCommand == null)
                    {
                        editMachineCommand = new RelayCommand(param => this.editMachine(), null);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ADS Error: Edit Machine Command", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                return editMachineCommand;
            }
        }
        private void editMachine()
        {
            Window editWindow = new Window();
            editWindow.Content = new MachineEditorView();
            editWindow.Title = "Machine Editor";
            editWindow.SizeToContent = SizeToContent.WidthAndHeight;
            editWindow.Show();
        }

        private void viewModule()
        {
            // see if this item is the currently selected Module in the ListView, if not simply skip (after refactoring I dont think it ever wont be the selected item, but cheap insurance)
            if (SelectedModule != null)
            {
                int tabIndex = this.CheckIfTabExists(SelectedModule.Name);

                if (tabIndex > -1 && tabIndex < TabItems.Count)
                {
                    SelectedTabIndex = tabIndex;
                }
                else
                {
                    SequenceEditorViewModel seVM = new SequenceEditorViewModel(SelectedModule, MyMachine);
                    SequenceEditorView view = new SequenceEditorView(seVM);

                    view.Name = SelectedModule.Name;

                    //check if the module sequence editor is already open?
                    TabItems.Add(view);

                    SelectedTabIndex = TabItems.Count - 1;
                }
            }
        }

        private int CheckIfTabExists(string tabName)
        {
            int result = -1;


            for (int i = 0; i < TabItems.Count; i++)
            {
                if (TabItems[i].Name.Equals(tabName))
                {
                    result = i;
                    break;
                }
            }

            return result;
        }

        private void closeModule()
        {
            if (SelectedTabIndex > 0 && SelectedTabIndex < TabItems.Count)
            {
                TabItems.RemoveAt(SelectedTabIndex);
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
