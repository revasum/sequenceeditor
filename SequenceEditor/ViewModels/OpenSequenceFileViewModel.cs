﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SequencerInterface.Definitions;

namespace SequenceEditor
{
    public class OpenSequenceFileViewModel : INotifyPropertyChanged
    {
        public OpenSequenceFileViewModel(string directory)
        {
            CurrentDirectory = directory;
        }

        private string currentDirectory = null;

        public string CurrentDirectory
        {
            get
            {
                return currentDirectory;
            }
            set
            {
                currentDirectory = value;
                OnPropertyChanged("CurrentDirectory");
            }
        }

        private ObservableCollection<SequenceFileInfo> sequenceFileInfoCollection = 
            new ObservableCollection<SequenceFileInfo>();

        public ObservableCollection<SequenceFileInfo> SequenceFileInfoCollection
        {
            get
            {
                return sequenceFileInfoCollection;
            }
            set
            {
                sequenceFileInfoCollection = value;
                OnPropertyChanged("SequenceFileInfoCollection");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
