﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using SequencerInterface.Definitions;
using SequencerInterface.Tools;

namespace SequenceEditor
{
    public class EnumEditorViewModel : INotifyPropertyChanged
    {
        private string fileDirectory;
        private string fileName;
        private ObservableCollection<EnumKeyValuePair> enumAsDict;
        public ObservableCollection<EnumKeyValuePair> EnumAsDict 
        {
            get { return enumAsDict; } 
            set
            {
                if (value == enumAsDict)
                    return;
                enumAsDict = value;
                OnPropertyChanged("EnumAsDict");
            }
        }

        public EnumEditorViewModel()
        {
            EnumAsDict = new ObservableCollection<EnumKeyValuePair>();
        }

        private void addRow()
        {
            EnumAsDict.Add(new EnumKeyValuePair("", 0));
        }

        private void newEnum()
        {
            EnumAsDict = new ObservableCollection<EnumKeyValuePair>();
            fileDirectory = "";
            fileName = "";
        }

        private void saveEnum()
        {
            XmlSerializerTools.
                Serializer<ObservableCollection<EnumKeyValuePair>>(EnumAsDict,
                                                                   fileDirectory,
                                                                   Defaults.DefaultSettings.EncryptFiles);
        }
        private void saveAsEnum()
        {
            Tools.SequencerXMLSerializerTools.
                SerializerWithSavePrompt<ObservableCollection<EnumKeyValuePair>>(EnumAsDict,
                                                                                 Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                 ".enum",
                                                                                 out fileDirectory,
                                                                                 out fileName);
        }

        private void openEnum()
        {
            EnumAsDict = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out fileDirectory,
                                                                                   out fileName,
                                                                                   out bool cancelled);
        }

        #region Commands
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(param => this.addRow(), null);
                return addCommand;
            }
        }
        private ICommand newCommand;
        public ICommand NewCommand
        {
            get
            {
                if (newCommand == null)
                    newCommand = new RelayCommand(param => this.newEnum(), null);
                return newCommand;
            }
        }
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                    saveCommand = new RelayCommand(param => this.saveEnum(), null);
                return saveCommand;
            }
        }
        private ICommand saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                if (saveAsCommand == null)
                    saveAsCommand = new RelayCommand(param => this.saveAsEnum(), null);
                return saveAsCommand;
            }
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get
            {
                if (openCommand == null)
                    openCommand = new RelayCommand(param => this.openEnum(), null);
                return openCommand;
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
