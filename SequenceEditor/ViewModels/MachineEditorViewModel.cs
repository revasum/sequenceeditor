﻿using System;
using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows;
using SequenceEditor.Definitions;

namespace SequenceEditor
{
    public class MachineEditorViewModel : INotifyPropertyChanged
    {
        private string fileDir, fileName;
        private bool unsavedChanges = false;

        public int SelectedIndex { get; set; }

        private SequenceEditorMachine myMachine;
        public SequenceEditorMachine MyMachine 
        {
            get { return myMachine; }
            set
            {
                if (myMachine == value) return;
                myMachine = value;
                OnPropertyChanged("MyMachine");
            }
        }

        public MachineEditorViewModel()
        {
            MyMachine = new SequenceEditorMachine();
        }

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(param => this.addModule(), null);
                return addCommand;
            }
        }
        private void addModule()
        {
            MyMachine.ModuleLocations.Add(new EnumKeyLocationPair());
        }

        private ICommand newCommand;
        public ICommand NewCommand
        {
            get
            {
                if (newCommand == null)
                    newCommand = new RelayCommand(param => this.newMachine(), null);
                return newCommand;
            }
        }
        private void newMachine()
        {
            MyMachine = new SequenceEditorMachine();
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get
            {
                if (openCommand == null)
                    openCommand = new RelayCommand(param => this.openMachine(), null);
                return openCommand;
            }
        }
        private void openMachine()
        {
            bool cancelled;

            Machine deserializedMachine = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<Machine>(Defaults.DefaultSequenceEditorDirectory + @"\Machines",
                                                    ".mac",
                                                    out fileDir,
                                                    out fileName,
                                                    out cancelled);
            if(!cancelled)
            {
                if (MyMachine == null)
                {
                    MyMachine = new SequenceEditorMachine();
                }

                if (deserializedMachine != null)
                {
                    MyMachine.CopyDeserializedModule(deserializedMachine);
                }
            }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                    saveCommand = new RelayCommand(param => this.save(), null);
                return saveCommand;
            }
        }
        private void save()
        {
            // Create a Machine object to serialize properly
            Machine serializedMachine = new Machine(MyMachine);

            XmlSerializerTools.
                Serializer<Machine>(serializedMachine,
                                    fileDir,
                                    Defaults.DefaultSettings.EncryptFiles);
        }

        private ICommand saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                if (saveAsCommand == null)
                    saveAsCommand = new RelayCommand(param => this.saveAsModule(), null);
                return saveAsCommand;
            }
        }
        private void saveAsModule()
        {
            // Create a Machine object to serialize properly
            Machine serializedMachine = new Machine(MyMachine);

            Tools.SequencerXMLSerializerTools.
                SerializerWithSavePrompt<Machine>(serializedMachine,
                                                  Defaults.DefaultSequenceEditorDirectory + @"\Machines",
                                                  ".mac",
                                                  out fileDir,
                                                  out fileName);
        }

        private ICommand changeModuleLocationCommand;
        public ICommand ChangeModuleLocationCommand
        {
            get
            {
                if (changeModuleLocationCommand == null)
                    changeModuleLocationCommand = new RelayCommand(param => this.changeModuleLocation(), null);
                return changeModuleLocationCommand;
            }
        }
        private void changeModuleLocation()
        {
            string selectedFile, selectedName;
            bool cancelled;

            Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<Module>(Defaults.DefaultSequenceEditorDirectory,
                                                   ".mod",
                                                   out selectedFile,
                                                   out selectedName,
                                                   out cancelled);

            if(!cancelled)
            {
                if (SelectedIndex < 0 || SelectedIndex >= MyMachine.ModuleLocations.Count)
                {
                    return;
                }

                MyMachine.ModuleLocations[SelectedIndex].Location = selectedFile;
            }
        }
        

        private ICommand exitCommand;
        public ICommand ExitCommand
        {
            get
            {
                if (exitCommand == null)
                    exitCommand = new RelayCommand(param => this.exit(), null);
                return exitCommand;
            }
        }
        private void exit()
        {
            if (unsavedChanges)
            {
                var result = System.Windows.MessageBox.Show("There are unsaved changes. Would you like to save now?", myMachine.Name, MessageBoxButton.YesNoCancel);
                if (result == System.Windows.MessageBoxResult.Yes)
                    throw new NotImplementedException();
                else if (result == MessageBoxResult.Cancel)
                    return;
            }
            System.Windows.Application.Current.Shutdown();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
