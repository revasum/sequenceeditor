﻿using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using SequenceEditor.Definitions;

namespace SequenceEditor
{
    public class SequenceEditorViewModel : INotifyPropertyChanged
    {
        private SequenceEditorModule myModule;

        private SequenceEditorMachine myMachine;

        public SequenceEditorModule MyModule
        {
            get { return myModule; }
            set
            {
                if (myModule == value) return;
                myModule = value;
                OnPropertyChanged("MyModule");
            }
        }

        public SequenceEditorViewModel(SequenceEditorMachine machine)
        {
            myMachine = machine;
            MyModule = new SequenceEditorModule(SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);
        }
        public SequenceEditorViewModel(SequenceEditorModule module, SequenceEditorMachine machine)
        {
            myMachine = machine;
            MyModule = module;
        }

        private ICommand newCommand;
        public ICommand NewCommand
        {
            get
            {
                if (newCommand == null)
                {
                    if (MyModule == null)
                        return null;
                    newCommand = new RelayCommand(param => MyModule.NewSequence(), null);
                }
                return newCommand;
            }
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get
            {
                if (openCommand == null)
                {
                    if (MyModule == null)
                    {
                        return null;
                    }

                    openCommand = new RelayCommand(param => MyModule.OpenSequence(), null);
                }
                return openCommand;
            }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    if (MyModule == null)
                        return null;
                    saveCommand = new RelayCommand(param => MyModule.SaveSequence(), null);
                }
                return saveCommand;
            }
        }
        private ICommand saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                if (saveAsCommand == null)
                {
                    if (MyModule == null)
                        return null;
                    saveAsCommand = new RelayCommand(param => MyModule.SaveAsSequence(), null);
                }
                return saveAsCommand;
            }
        }

        private ICommand uploadCommand;
        public ICommand UploadCommand
        {
            get
            {
                if (uploadCommand == null)
                    uploadCommand = new RelayCommand(param => this.MyModule.UploadSequence(), null);
                return uploadCommand;
            }
        }
        private ICommand runCommand;
        public ICommand RunCommand
        {
            get
            {
                if (runCommand == null)
                    runCommand = new RelayCommand(param => this.MyModule.RunSequence(), null);
                return runCommand;
            }
        }
        private ICommand nextStepCommand;
        public ICommand NextStepCommand
        {
            get
            {
                if (nextStepCommand == null)
                    nextStepCommand = new RelayCommand(param => this.MyModule.NextStepSequence(), null);
                return nextStepCommand;
            }
        }
        private ICommand abortCommand;
        public ICommand AbortCommand
        {
            get
            {
                if (abortCommand == null)
                {
                    abortCommand = new RelayCommand(param => this.MyModule.AbortSequence(), null);
                }

                return abortCommand;
            }
        }
        private ICommand resetCommand;
        public ICommand ResetCommand
        {
            get
            {
                if (resetCommand == null)
                    resetCommand = new RelayCommand(param => this.MyModule.ResetSequence(), null);
                return resetCommand;
            }
        }

        private ICommand executeCommand;
        public ICommand ExecuteCommand
        {
            get
            {
                if(executeCommand == null)
                {
                    executeCommand = new RelayCommand(parm => this.MyModule.ExecuteSequence(myMachine), null);
                }
                return executeCommand;
            }
        }

        private ICommand printCommand;
        public ICommand PrintCommand
        {
            get
            {
                if (printCommand == null)
                    printCommand = new RelayCommand(param => this.print(), null);
                return printCommand;
            }
        }

        PrintDocument recordDoc = new PrintDocument();
        private void print()
        {
            // Create the document and name it
            recordDoc= new PrintDocument();
            recordDoc.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
            // Preview document
            PrintDialog dlg = new PrintDialog();
            dlg.Document = recordDoc;
            if (dlg.ShowDialog() == DialogResult.OK)
                recordDoc.Print();
        }
        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            string message = MyModule.CurrentSequenceTable.Summary();
            // Print receipt
            Font myFont = new Font("Times New Roman", 11);
            e.Graphics.DrawString(message , myFont, Brushes.Black, 50, 50);
        }

        private ICommand exportSummaryCommand;
        public ICommand ExportSummaryCommand
        {
            get
            {
                if (exportSummaryCommand == null)
                    exportSummaryCommand = new RelayCommand(param => this.exportSummary(), null);
                return exportSummaryCommand;
            }
        }
        private void exportSummary()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            dlg.InitialDirectory = @"C:\SequenceEditor\Summaries";

            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "(" + ".txt" + ")|*" + ".txt"; // Filter files by extension 

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                try
                {
                    System.IO.File.WriteAllText(dlg.FileName, MyModule.CurrentSequenceTable.Summary(), Encoding.UTF8);
                }
                catch(Exception e)
                {
                    MessageBox.Show("Could not export summary.\n" + e.Message);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
