﻿using SequencerInterface.Tools;
using SequencerInterface.Definitions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace SequenceEditor
{
    public class CascadingEnumEditorViewModel : INotifyPropertyChanged
    {
        private string fileDirectory;
        private string fileName;
        public String FileName
        {
            get { return fileName; }
            set
            {
                if (value == fileName)
                    return;
                fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        private string parentFileName;
        public String ParentFileName
        {
            get { return parentFileName; }
            set
            {
                if (value == parentFileName)
                    return;
                parentFileName = value;
                OnPropertyChanged("ParentFileName");
            }
        }

        private string childFileName;
        public String ChildFileName
        {
            get { return childFileName; }
            set
            {
                if (value == childFileName)
                    return;
                childFileName = value;
                OnPropertyChanged("ChildFileName");
            }
        }

        private ObservableCollection<EnumKeyValuePair> parentEnumList;
        public ObservableCollection<EnumKeyValuePair> ParentEnumList
        {
            get { return parentEnumList; }
            set
            {
                if (value == parentEnumList)
                    return;
                parentEnumList = value;
                OnPropertyChanged("ParentEnumList");
            }
        }

        private ObservableCollection<EnumKeyValuePair> childEnumList;
        public ObservableCollection<EnumKeyValuePair> ChildEnumList
        {
            get { return childEnumList; }
            set
            {
                if (value == childEnumList)
                    return;
                childEnumList = value;
                OnPropertyChanged("ChildEnumList");
            }
        }

        private ObservableCollection<EnumKeyLocationPair> cascadingEnumList;
        public ObservableCollection<EnumKeyLocationPair> CascadingEnumList
        {
            get { return cascadingEnumList; }
            set
            {
                if (value == cascadingEnumList)
                    return;
                cascadingEnumList = value;
                OnPropertyChanged("CascadingEnumList");
            }
        }

        private EnumKeyLocationPair selectedItem;
        public EnumKeyLocationPair SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == selectedItem)
                    return;
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public CascadingEnumEditorViewModel()
        {
            CascadingEnumList = new ObservableCollection<EnumKeyLocationPair>();
            CascadingEnumList.Add(new EnumKeyLocationPair());

            ParentEnumList = new ObservableCollection<EnumKeyValuePair>();
            ParentEnumList.Add(new EnumKeyValuePair("door", 1));
            ParentEnumList.Add(new EnumKeyValuePair("spray", 2));
            ParentEnumList.Add(new EnumKeyValuePair("cleaner", 3));
            ParentEnumList.Add(new EnumKeyValuePair("probe", 4));

            ChildEnumList = new ObservableCollection<EnumKeyValuePair>();
            ChildEnumList.Add(new EnumKeyValuePair("up", 1));
            ChildEnumList.Add(new EnumKeyValuePair("down", 2));
            ChildEnumList.Add(new EnumKeyValuePair("on", 3));
            ChildEnumList.Add(new EnumKeyValuePair("off", 4));
            ChildEnumList.Add(new EnumKeyValuePair("zero", 5));
            ChildEnumList.Add(new EnumKeyValuePair("extend", 6));
            ChildEnumList.Add(new EnumKeyValuePair("retract", 7));
        }

        private void addRow()
        {
            CascadingEnumList.Add(new EnumKeyLocationPair());
        }

        private void newEnum()
        {
            CascadingEnumList = new ObservableCollection<EnumKeyLocationPair>();
            fileDirectory = "";
            FileName = "";
        }

        private void saveEnum()
        {
            XmlSerializerTools.
                Serializer<ObservableCollection<EnumKeyLocationPair>>(CascadingEnumList,
                                                                      fileDirectory,
                                                                      Defaults.DefaultSettings.EncryptFiles);
        }
        private void saveAsEnum()
        {
            Tools.SequencerXMLSerializerTools.
                SerializerWithSavePrompt<ObservableCollection<EnumKeyLocationPair>>(CascadingEnumList,
                                                                                    Defaults.DefaultSequenceEditorDirectory,
                                                                                    ".rel",
                                                                                    out fileDirectory,
                                                                                    out fileName);
            FileName = fileName;
        }

        private void openEnum()
        {
            string fileN;
            bool cancelled;

            var tempDict = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyLocationPair>>(Defaults.DefaultSequenceEditorDirectory + @"\EnumRelationships",
                                                                                      ".rel",
                                                                                      out fileDirectory,
                                                                                      out fileN,
                                                                                      out cancelled);

            if(!cancelled && (tempDict != null))
            {
                FileName = fileN; 
                CascadingEnumList.Clear();
                foreach (var entry in tempDict)
                    CascadingEnumList.Add(entry);
            }
        }


        private void openParentEnum()
        {
            string fileN;
            string fileD;
            bool cancelled;

            var tempDict = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out fileD,
                                                                                   out fileN,
                                                                                   out cancelled);

            if (!cancelled && (tempDict != null))
            {
                ParentFileName = fileN;
                ParentEnumList.Clear();
                foreach (var entry in tempDict)
                    ParentEnumList.Add(entry);
            }
        }
        private void openChildEnum()
        {
            string fileN;
            string fileD;
            bool cancelled;

            var tempDict = Tools.SequencerXMLSerializerTools.
                DeserializerWithOpenPrompt<ObservableCollection<EnumKeyValuePair>>(Defaults.DefaultSequenceEditorDirectory + @"\Enums",
                                                                                   ".enum",
                                                                                   out fileD,
                                                                                   out fileN,
                                                                                   out cancelled);

            if (!cancelled && (tempDict != null))
            {
                ChildFileName = fileN;
                ChildEnumList.Clear();
                foreach (var entry in tempDict)
                    ChildEnumList.Add(entry);
            }
        }

        #region Commands
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                    addCommand = new RelayCommand(param => this.addRow(), null);
                return addCommand;
            }
        }
        private ICommand newCommand;
        public ICommand NewCommand
        {
            get
            {
                if (newCommand == null)
                    newCommand = new RelayCommand(param => this.newEnum(), null);
                return newCommand;
            }
        }
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                    saveCommand = new RelayCommand(param => this.saveEnum(), null);
                return saveCommand;
            }
        }
        private ICommand saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                if (saveAsCommand == null)
                    saveAsCommand = new RelayCommand(param => this.saveAsEnum(), null);
                return saveAsCommand;
            }
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get
            {
                if (openCommand == null)
                    openCommand = new RelayCommand(param => this.openEnum(), null);
                return openCommand;
            }
        }
        private ICommand openParentCommand;
        public ICommand OpenParentCommand
        {
            get
            {
                if (openParentCommand == null)
                    openParentCommand = new RelayCommand(param => this.openParentEnum(), null);
                return openParentCommand;
            }
        }
        private ICommand openChildCommand;
        public ICommand OpenChildCommand
        {
            get
            {
                if (openChildCommand == null)
                    openChildCommand = new RelayCommand(param => this.openChildEnum(), null);
                return openChildCommand;
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
