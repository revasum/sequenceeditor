﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using System.Xml;
using System.Security.Cryptography;
using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using SequenceEditor.Definitions;

namespace SequenceEditor.Tools
{
    public class SequenceFileFinder
    {
        #region Member Variables

        private List<SequenceFileInfo> m_SequenceFileInfoList = new List<SequenceFileInfo>();

        private SequenceEditorModule m_Module = null;

        private SequenceFileScanProgressDlg m_SequenceFileScanProgressDlg = new SequenceFileScanProgressDlg();

        private BackgroundWorker m_SequenceFileProcessor = new BackgroundWorker();

        private OpenSequenceFileViewModel m_OpenSequenceViewModel = null;


        #endregion Member Variables

        #region Properties

        public string CurrentSelectedFile { get; set; } = null;

        #endregion Properties

        #region Constructor

        public SequenceFileFinder(SequenceEditorModule module)
        {
            m_Module = module;

            m_OpenSequenceViewModel = new OpenSequenceFileViewModel(module.CurrentWorkingDirectory);

            m_SequenceFileProcessor.DoWork += new DoWorkEventHandler(SequenceFileProcessor_DoWork);
            m_SequenceFileProcessor.ProgressChanged += new ProgressChangedEventHandler(SequenceFileProcessor_ProgressChanged);
            m_SequenceFileProcessor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(SequenceFileProcessor_RunWorkerCompleted);
            m_SequenceFileProcessor.WorkerReportsProgress = true;
        }

        #endregion Constructor

        #region UI Methods

        public void ShowOpenSequenceFileWindow()
        {
            OpenSequenceFileView openSequenceFileView = new OpenSequenceFileView(Application.Current.MainWindow, m_OpenSequenceViewModel, this);
            openSequenceFileView.ShowDialog();
        }

        public string SelectSequenceFilesDirectory()
        {
            string sequenceFilesPath = null;

            System.Windows.Forms.FolderBrowserDialog selectSequenceDirectoryDialog  = new System.Windows.Forms.FolderBrowserDialog();

            // Set the help text description for the FolderBrowserDialog.
            selectSequenceDirectoryDialog.Description = "Select the directory that contains the sequence files you wish to use.";

            // Do not allow the user to create new files via the FolderBrowserDialog.
            selectSequenceDirectoryDialog.ShowNewFolderButton = false;

            // Default to the root sequence folder
            selectSequenceDirectoryDialog.SelectedPath = m_Module.CurrentWorkingDirectory;

            System.Windows.Forms.DialogResult result = selectSequenceDirectoryDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                sequenceFilesPath = selectSequenceDirectoryDialog.SelectedPath;

                // persist the path so we dont have to repeatedly select it
                SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory = sequenceFilesPath;
                SequenceEditor.Properties.Settings.Default.Save();
            }

            return sequenceFilesPath;
        }

        #endregion UI Methods

        

        #region BackgroundWorker Handling

        public void StartParsingFileInfoFromDirectory(string directory)
        {
            if( !m_SequenceFileProcessor.IsBusy )
            {
                m_OpenSequenceViewModel.SequenceFileInfoCollection.Clear();
                m_Module.CurrentWorkingDirectory = directory;
                m_SequenceFileProcessor.RunWorkerAsync();
                m_SequenceFileScanProgressDlg.ShowDialog();
            }
        }

        private void SequenceFileProcessor_DoWork(object sender, DoWorkEventArgs e)
        {
            
            // Clear the sequence file info list to receive new directory data
            m_SequenceFileInfoList.Clear();

            int fileCount = 0;

            string[] fileList = Directory.GetFiles(m_Module.CurrentWorkingDirectory, "*.seq", SearchOption.TopDirectoryOnly);

            foreach (string file in fileList)
            {
                if (ModuleTypeMatch(file))
                {
                    m_SequenceFileInfoList.Add(new SequenceFileInfo(file, Path.GetFileNameWithoutExtension(file)));

                    m_SequenceFileProcessor.ReportProgress(GetPercentageComplete(++fileCount, fileList.Length));
                }
            }
        }

        private bool ModuleTypeMatch( string file)
        {
            bool match = false;

            const string SALT = "##8r4!Il";

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(file);

                XmlNode node = xmlDoc.SelectSingleNode("/SequenceTableStructure/ModuleTypeName");

                if (node.InnerText.Equals(m_Module.ModuleTypeName, StringComparison.CurrentCultureIgnoreCase))
                {
                    match = true;
                }
                else
                {
                    match = false;
                }
            }
            catch
            {
                try
                {
                    string fileData = File.ReadAllText(file);

                    fileData = Encryption64.Decrypt(fileData, SALT);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(fileData);

                    XmlNode node = xmlDoc.SelectSingleNode("/SequenceTableStructure/ModuleTypeName");

                    if (node.InnerText.Equals(m_Module.ModuleTypeName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        match = true;
                    }
                }
                catch
                {
                    // Skip
                }
            }

            return match;
        }

        private void SequenceFileProcessor_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            m_SequenceFileScanProgressDlg.SetProgressBarPercentage( e.ProgressPercentage );
        }

        private void SequenceFileProcessor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_SequenceFileScanProgressDlg.Close();
            m_OpenSequenceViewModel.SequenceFileInfoCollection = new ObservableCollection<SequenceFileInfo>(m_SequenceFileInfoList);
        }

        private int GetPercentageComplete( int count, int maxCount )
        {
            double percentageComplete = count / (double)maxCount;

            return (int)(percentageComplete * 100);
        }

        #endregion BackgroundWorker Handling
    }
}
