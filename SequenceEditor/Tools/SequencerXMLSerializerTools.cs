﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SequencerInterface.Tools;
using SequencerInterface.Definitions;

namespace SequenceEditor.Tools
{

    public static class SequencerXMLSerializerTools
    {
        public static T DeserializerWithOpenPrompt<T>(string initDir, string defaultExt, out string fileDir, out string fileName, out bool cancelled)
        {
            fileDir = null;
            fileName = null;
            cancelled = false;

            System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();

            openFileDialog1.InitialDirectory = initDir;
            openFileDialog1.Filter = "(" + defaultExt + ")|*" + defaultExt;  // Filter files by extension 
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            System.Windows.Forms.DialogResult result = openFileDialog1.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                fileDir = openFileDialog1.FileName;
                fileName = openFileDialog1.SafeFileName;

                return XmlSerializerTools.Deserializer<T>(fileDir);
            }
            else if(result == System.Windows.Forms.DialogResult.Cancel)
            {
                cancelled = true;
            }

            return default(T);
        }


        public static void SerializerWithSavePrompt<T>(T data,
                                                       string initDir,
                                                       string defaultExt,
                                                       out string fileDir,
                                                       out string fileName)
        {
            fileDir = null;
            fileName = null;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            dlg.InitialDirectory = initDir;

            dlg.DefaultExt = defaultExt; // Default file extension
            dlg.Filter = "(" + defaultExt + ")|*" + defaultExt; // Filter files by extension 

            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                if (typeof(T) == typeof(SequencerInterface.Definitions.SequenceTableStructure))
                    (data as SequencerInterface.Definitions.SequenceTableStructure).Name = dlg.SafeFileName;

                fileDir = dlg.FileName;
                fileName = dlg.SafeFileName;
                XmlSerializerTools.Serializer<T>(data, fileDir, Defaults.DefaultSettings.EncryptFiles);
            }
        }
    }
}
