﻿using System.Windows.Forms;

namespace SequenceEditor.Tools
{
    partial class SequenceFileScanProgressDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_SequenceFileScanProgressBar = new System.Windows.Forms.ProgressBar();
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_SequenceFileScanProgressBar
            // 
            this.m_SequenceFileScanProgressBar.Location = new System.Drawing.Point(16, 12);
            this.m_SequenceFileScanProgressBar.Name = "m_SequenceFileScanProgressBar";
            this.m_SequenceFileScanProgressBar.Size = new System.Drawing.Size(525, 23);
            this.m_SequenceFileScanProgressBar.TabIndex = 0;
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.Location = new System.Drawing.Point(241, 52);
            this.m_btnCancel.Name = "Cancel";
            this.m_btnCancel.Size = new System.Drawing.Size(75, 23);
            this.m_btnCancel.TabIndex = 1;
            this.m_btnCancel.Text = "Cancel";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            // 
            // SequenceFileScanProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 87);
            this.Controls.Add(this.m_btnCancel);
            this.Controls.Add(this.m_SequenceFileScanProgressBar);
            this.Name = "SequenceFileScanProgress";
            this.Text = "Sequence File Scan Progress";
            this.ResumeLayout(false);

        }

        public void SetProgressBarPercentage( int percentage )
        {
            if(( percentage >= 0) && (percentage <= 100))
            {
                m_SequenceFileScanProgressBar.Value = percentage;
            }
            else
            {
                MessageBox.Show("Progress Bar Percentage out of range");
            }
        }

        #endregion

        private System.Windows.Forms.ProgressBar m_SequenceFileScanProgressBar;
        private System.Windows.Forms.Button m_btnCancel;
    }
}