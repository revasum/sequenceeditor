﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System.ComponentModel;
using SequenceEditor.Tools;

namespace SequenceEditor.Definitions
{
    public class SequenceEditorModule : Module, INotifyPropertyChanged
    {
        protected string m_SaveFileName = null;


        public SequenceEditorModule() : base()
        {

        }

        public SequenceEditorModule(String currentWorkingDirectory) : base(currentWorkingDirectory)
        {

        }

        public SequenceEditorModule(Module rhs, String currentWorkingDirectory) : base(rhs, currentWorkingDirectory)
        {

        }

        #region Copy Constructor
        public void CopyDeserializedModule(Module deserializedModule)
        {
            Name = deserializedModule.Name;
            EndpointsFileLocation = deserializedModule.EndpointsFileLocation;
            ObjectsFileLocation = deserializedModule.ObjectsFileLocation;
            CommandsFileLocation = deserializedModule.CommandsFileLocation;
            SequenceStepFlagFileLocation = deserializedModule.SequenceStepFlagFileLocation;
            ObjectCommandCascadeFileLocation = deserializedModule.ObjectCommandCascadeFileLocation;
            AxisSetPointDefinitionsFileLocation = deserializedModule.AxisSetPointDefinitionsFileLocation;
            CurrentWorkingDirectory = deserializedModule.CurrentWorkingDirectory;
            SequencerObjectID = deserializedModule.SequencerObjectID;
            PlcModuleLocation = deserializedModule.PlcModuleLocation;
            SequenceTableLocation = deserializedModule.SequenceTableLocation;
            SequencerStatusLocation = deserializedModule.SequencerStatusLocation;
            AdsIpAddress = deserializedModule.AdsIpAddress;
            ModuleTypeName = deserializedModule.ModuleTypeName;
            NullBytesAfterTime = deserializedModule.NullBytesAfterTime;
            NullBytesAfterEndpoints = deserializedModule.NullBytesAfterEndpoints;
            NullBytesAfterCommands = deserializedModule.NullBytesAfterCommands;
            NullBytesAfterAxes = deserializedModule.NullBytesAfterAxes;
            NumberOfSteps = deserializedModule.NumberOfSteps;
            NumberOfEndpointGroups = deserializedModule.NumberOfEndpointGroups;
            NumberOfCommandGroups = deserializedModule.NumberOfCommandGroups;
            NumberOfAxes = deserializedModule.NumberOfAxes;
            NumberOfStepFlags = deserializedModule.NumberOfStepFlags;
            NumberOfProcessFlags = deserializedModule.NumberOfProcessFlags;
        }


        #endregion Copy Constructor

        #region Properties

        public override string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value == name) return;
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public override string EndpointsFileLocation
        {
            get { return endpointsFileLocation; }
            set
            {
                if (endpointsFileLocation == value) return;
                endpointsFileLocation = value;
                SequencerModuleDefinitions.Endpoints = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
                OnPropertyChanged("EndpointsFileLocation");
            }
        }
        public override string ObjectsFileLocation
        {
            get { return objectsFileLocation; }
            set
            {
                if (objectsFileLocation == value) return;
                objectsFileLocation = value;
                SequencerModuleDefinitions.Objects = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
                OnPropertyChanged("ObjectsFileLocation");
            }
        }
        public override string CommandsFileLocation
        {
            get { return commandsFileLocation; }
            set
            {
                if (commandsFileLocation == value) return;
                commandsFileLocation = value;
                SequencerModuleDefinitions.Commands = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
                OnPropertyChanged("CommandsFileLocation");
            }
        }
        public override string SequenceStepFlagFileLocation
        {
            get { return sequenceStepFlagFileLocation; }
            set
            {
                if (sequenceStepFlagFileLocation == value) return;
                sequenceStepFlagFileLocation = value;
                SequencerModuleDefinitions.SequenceStepFlags = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
                OnPropertyChanged("SequenceStepFlagFileLocation");
            }
        }
        public override string SequenceProcessFlagFileLocation
        {
            get { return sequenceProcessFlagFileLocation; }
            set
            {
                if (sequenceProcessFlagFileLocation == value) return;
                sequenceProcessFlagFileLocation = value;
                SequencerModuleDefinitions.SequenceProcessFlags = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyValuePair>>(value);
                OnPropertyChanged("SequenceProcessFlagFileLocation");
            }
        }
        public override string ObjectCommandCascadeFileLocation
        {
            get { return objectCommandCascadeFileLocation; }
            set
            {
                if (objectCommandCascadeFileLocation == value) return;
                objectCommandCascadeFileLocation = value;
                SequencerModuleDefinitions.ObjectCommandCascade = XmlSerializerTools.Deserializer<ObservableCollection<EnumKeyLocationPair>>(value);
                OnPropertyChanged("ObjectCommandCascadeFileLocation");
            }
        }
        public override string AxisSetPointDefinitionsFileLocation
        {
            get { return axisSetPointDefinitionsFileLocation; }
            set
            {
                if (axisSetPointDefinitionsFileLocation == value) return;
                axisSetPointDefinitionsFileLocation = value;
                SequencerModuleDefinitions.AxisSetPointDefinitions = XmlSerializerTools.Deserializer<ObservableCollection<AxisDefinition>>(value);
                OnPropertyChanged("AxisSetPointDefinitionsFileLocation");
            }
        }

        public override SequenceTableStructure CurrentSequenceTable
        {
            get { return currentSequenceTable; }
            set
            {
                if (currentSequenceTable == value) return;
                currentSequenceTable = value;
                OnPropertyChanged("CurrentSequenceTable");
            }
        }

        public override bool Connected
        {
            get { return connected; }
            set
            {
                if (connected == value) return;
                connected = value;
                OnPropertyChanged("Connected");
            }
        }

        public override SequencerStatus LiveSequencerStatus
        {
            get { return liveSequencerStatus; }
            set
            {
                if (liveSequencerStatus == value) return;
                liveSequencerStatus = value;
                OnPropertyChanged("LiveSequencerStatus");
            }
        }

        public override ModuleDefinitions SequencerModuleDefinitions
        {
            get { return sequencerModuleDefinitions; }
            set
            {
                if (sequencerModuleDefinitions == value) return;
                sequencerModuleDefinitions = value;
                OnPropertyChanged("SequencerModuleDefinitions");
            }
        }
        
        public override string ErrorMessages
        {
            get 
            { 
                    return errorMessages; 
            }
            set
            {
                if (value == errorMessages)
                {
                    return;
                }

                errorMessages = value;
                OnPropertyChanged("ErrorMessages");
            }
        }
        #region Sequence table structure and size variables

        public override SequencerType SequencerObjectID
        {
            get
            {
                return sequencerObjectID;
            }
            set
            {
                if(sequencerObjectID != value)
                {
                    sequencerObjectID = value;
                    OnPropertyChanged("SequencerObjectID");
                }
            }
        }

        public override string PlcModuleLocation
        {
            get
            {
                return plcModuleLocation;
            }
            set
            {
                if (plcModuleLocation != value)
                {
                    plcModuleLocation = value;
                    OnPropertyChanged("PlcModuleLocation");
                }
            }
        }

        public override string SequenceTableLocation
        {
            get
            {
                return sequenceTableLocation;
            }
            set
            {
                if (sequenceTableLocation != value)
                {
                    sequenceTableLocation = value;
                    OnPropertyChanged("SequenceTableLocation");
                }
            }
        }

        public override string SequencerStatusLocation
        {
            get
            {
                return sequencerStatusLocation;
            }
            set
            {
                if (sequencerStatusLocation != value)
                {
                    sequencerStatusLocation = value;
                    OnPropertyChanged("SequencerStatusLocation");
                }
            }
        }

        public override string AdsIpAddress
        {
            get
            {
                return adsIpAddress;
            }
            set
            {
                //set if module resides on private plc
                if (adsIpAddress != value)
                {
                    adsIpAddress = value;
                    OnPropertyChanged("AdsIpAddress");
                }
            }
        }

        public override string ModuleTypeName // Sequence tables can be shared between modules with the same ModuleTypeName.
        {
            get
            {
                return moduleTypeName;
            }
            set
            {
                if (moduleTypeName != value)
                {
                    moduleTypeName = value;
                    OnPropertyChanged("ModuleTypeName");
                }
            }
        }

        public override int NullBytesAfterTime
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterTime;
            }
            set
            {
                if (SequencerModuleDefinitions.NullBytesAfterTime != value)
                {
                    SequencerModuleDefinitions.NullBytesAfterTime = value;
                    OnPropertyChanged("NullBytesAfterTime");
                }
            }
        }
        public override int NullBytesAfterEndpoints
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterEndpoints;
            }
            set
            {
                if (SequencerModuleDefinitions.NullBytesAfterEndpoints != value)
                {
                    SequencerModuleDefinitions.NullBytesAfterEndpoints = value;
                    OnPropertyChanged("NullBytesAfterEndpoints");
                }
            }
        }
        public override int NullBytesAfterCommands
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterCommands;
            }
            set
            {
                if (SequencerModuleDefinitions.NullBytesAfterCommands != value)
                {
                    SequencerModuleDefinitions.NullBytesAfterCommands = value;
                    OnPropertyChanged("NullBytesAfterCommands");
                }
            }
        }
        public override int NullBytesAfterAxes
        {
            get
            {
                return SequencerModuleDefinitions.NullBytesAfterAxes;
            }
            set
            {
                if (SequencerModuleDefinitions.NullBytesAfterAxes != value)
                {
                    SequencerModuleDefinitions.NullBytesAfterAxes = value;
                    OnPropertyChanged("NullBytesAfterAxes");
                }
            }
        }

        //amount of each group for this module
        public override int NumberOfSteps
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfSteps;
            }
            set
            {
                if (SequencerModuleDefinitions.NumberOfSteps != value)
                {
                    SequencerModuleDefinitions.NumberOfSteps = value;
                    OnPropertyChanged("NumberOfSteps");
                }
            }
        }
        public override int NumberOfEndpointGroups
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfEndpointGroups;
            }
            set
            {
                if (SequencerModuleDefinitions.NumberOfEndpointGroups != value)
                {
                    SequencerModuleDefinitions.NumberOfEndpointGroups = value;
                    OnPropertyChanged("NumberOfEndpointGroups");
                }
            }
        }
        public override int NumberOfCommandGroups
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfCommandGroups;
            }
            set
            {
                if (SequencerModuleDefinitions.NumberOfCommandGroups != value)
                {
                    SequencerModuleDefinitions.NumberOfCommandGroups = value;
                    OnPropertyChanged("NumberOfCommandGroups");
                }
            }
        }
        public override int NumberOfAxes
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfAxes;
            }
            set
            {
                if (SequencerModuleDefinitions.NumberOfAxes != value)
                {
                    SequencerModuleDefinitions.NumberOfAxes = value;
                    OnPropertyChanged("NumberOfAxes");
                }
            }
        }
        public override int NumberOfStepFlags
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfStepFlags;
            }
            set
            {
                if (SequencerModuleDefinitions.NumberOfStepFlags != value)
                {
                    SequencerModuleDefinitions.NumberOfStepFlags = value;
                    OnPropertyChanged("NumberOfStepFlags");
                }
            }
        }
        public override int NumberOfProcessFlags
        {
            get
            {
                return SequencerModuleDefinitions.NumberOfProcessFlags;
            }
            set
            {
                if (SequencerModuleDefinitions.NumberOfProcessFlags != value)
                {
                    SequencerModuleDefinitions.NumberOfProcessFlags = value;
                    OnPropertyChanged("NumberOfProcessFlags");
                }
            }
        }

        #endregion


        #endregion Properties

        private bool ModuleTypeFailsToMatch(SequenceTableStructure table)
        {
            if (ModuleTypeName != null)
            {
                // A pre-existing sequence table may not have ModuleTypeName set.  Accept it.
                if (table.ModuleTypeName == null) return false;

                // Accept any sequence table if the table has no type name.
                if (table.ModuleTypeName.Length == 0) return false;

                // Accept the sequence table if the type name matches.  (Case insensitive.)
                if (table.ModuleTypeName.Equals(ModuleTypeName, StringComparison.CurrentCultureIgnoreCase)) return false;
            }
            else
            {
                throw new InvalidOperationException("Invalid Sequencer Module Type Name");
            }

            // If type name does not match then reject this sequence table.
            return true;
        }

        public SequenceTableStructure OpenSequence()
        {
            if (SequencerModuleDefinitions != null)
            {
                SequenceFileFinder sequenceFileFinder = new SequenceFileFinder(this);

                sequenceFileFinder.ShowOpenSequenceFileWindow();

                if (sequenceFileFinder.CurrentSelectedFile != null)
                {
                    CurrentWorkingDirectory = Path.GetDirectoryName(sequenceFileFinder.CurrentSelectedFile);
                    CurrentSequenceTable = XmlSerializerTools.Deserializer<SequenceTableStructure>(sequenceFileFinder.CurrentSelectedFile);

                    if (CurrentSequenceTable != null)
                    {
                        CurrentSequenceTable.Definitions = SequencerModuleDefinitions;
                    }

                    m_SaveFileLocation = sequenceFileFinder.CurrentSelectedFile;
                }
            }
            else
            {
                throw new InvalidOperationException("Invalid Sequencer Module Defintions");
            }

            return CurrentSequenceTable;
        }

        public void SaveSequence()
        {
            if (m_SaveFileLocation != null)
            {
                if (ModuleTypeName != null)
                {
                    if (CurrentSequenceTable != null)
                    {
                        CurrentSequenceTable.ModuleTypeName = ModuleTypeName;
                        XmlSerializerTools.Serializer<SequenceTableStructure>(CurrentSequenceTable,
                                                                              m_SaveFileLocation,
                                                                              Defaults.DefaultSettings.EncryptFiles);
                    }
                    else
                    {
                        throw new InvalidOperationException("Invalid Current Sequence Table");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Invalid Sequencer Module Type Name");
                }
            }
            else
            {
                SaveAsSequence();
            }
        }

        public void SaveAsSequence()
        {
            if (ModuleTypeName != null)
            {
                if (CurrentSequenceTable != null)
                {
                    CurrentSequenceTable.ModuleTypeName = ModuleTypeName;
                    SequencerXMLSerializerTools.
                        SerializerWithSavePrompt<SequenceTableStructure>(CurrentSequenceTable,
                                                                         Path.GetDirectoryName(m_SaveFileLocation),
                                                                         ".seq",
                                                                         out m_SaveFileLocation,
                                                                         out m_SaveFileName);
                    CurrentSequenceTable.Name = m_SaveFileName;
                }
                else
                {
                    throw new InvalidOperationException("Invalid Current Sequence Table");
                }
            }
            else
            {
                throw new InvalidOperationException("Invalid Sequencer Module Type Name");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
