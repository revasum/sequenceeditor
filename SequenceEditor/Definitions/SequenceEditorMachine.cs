﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using TwinCAT.Ads;

namespace SequenceEditor.Definitions
{
    public class SequenceEditorMachine : Machine, INotifyPropertyChanged
    {
        public SequenceEditorMachine()
        {
            sequenceEditorModules = new ObservableCollection<SequenceEditorModule>();
        }

        #region Copy Constructor

        public void CopyDeserializedModule(Machine deserializedMachine)
        {
            Name = deserializedMachine.Name;
            AdsIpAddress = deserializedMachine.AdsIpAddress;
            CommandHandshakeStyle = deserializedMachine.CommandHandshakeStyle;
            ModuleLocations = deserializedMachine.ModuleLocations;
        }

        #endregion

        #region Properties
        public override string Name
        {
            get { return name; }
            set
            {
                if (value == name) return;
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public override string AdsIpAddress
        {
            get { return adsIpAddress; }
            set
            {
                if (value == adsIpAddress) return;
                adsIpAddress = value;
                OnPropertyChanged("AdsIpAddress");
            }
        }

        public override ObservableCollection<EnumKeyLocationPair> ModuleLocations
        {
            get
            {
                return moduleLocations;
            }
            set
            {
                moduleLocations = value;
                OnPropertyChanged("ModuleLocations");
            }
        }


        [System.Xml.Serialization.XmlIgnore()]
        public override StateInfo AdsState
        {
            get { return adsState; }
            set
            {
                if (adsState.Equals(value)) return;
                adsState = value;
                OnPropertyChanged("AdsState");
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public override bool Connected
        {
            get { return connected; }
            set
            {
                if (value == connected) return;
                connected = value;
                OnPropertyChanged("Connected");
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public override bool EncryptFiles
        {
            get
            {
                return _encryptedFiles;
            }
            set
            {
                if (_encryptedFiles == value)
                    return;
                _encryptedFiles = value;
                OnPropertyChanged("EncryptFiles");

                Defaults.DefaultSettings.EncryptFiles = value;
                Defaults.SaveDefaultSettings();
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public override bool UseBlockTransfer
        {
            get
            {
                return _useBlockTransfer;
            }
            set
            {
                if (_useBlockTransfer == value)
                    return;
                _useBlockTransfer = value;
                OnPropertyChanged("UseBlockTransfer");

                Defaults.DefaultSettings.UseBlockTransfer = value;
                Defaults.SaveDefaultSettings();
            }
        }


        private ObservableCollection<SequenceEditorModule> sequenceEditorModules;
        [System.Xml.Serialization.XmlIgnore()]
        public ObservableCollection<SequenceEditorModule> SequenceEditorModules
        {
            get { return sequenceEditorModules; }
            set
            {
                if (sequenceEditorModules == value) return;
                sequenceEditorModules = value;
                OnPropertyChanged("Modules");
            }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public override AmsRouterState RouterState
        {
            get { return routerState; }
            set
            {
                if (routerState.Equals(value)) return;
                routerState = value;
                OnPropertyChanged("RouterState");
            }
        }

        protected override void ConnectModules()
        {
            // refactored it so if one module fails, it stops trying
            try
            {
                foreach (var module in SequenceEditorModules)
                {
                    module.Connect(adsTcClient, this.CommandHandshakeStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connect Modules Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        #endregion Properties

        public override void Init()
        {
            foreach (var modLocation in ModuleLocations)
            {
                Module deserializedModule = XmlSerializerTools.Deserializer<Module>(modLocation.Location);

                deserializedModule.CurrentWorkingDirectory = SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory;

                if(deserializedModule != null)
                {
                    SequenceEditorModule sequenceEditorModule = new SequenceEditorModule(SequenceEditor.Properties.Settings.Default.CurrentWorkingDirectory);
                    sequenceEditorModule.CopyDeserializedModule(deserializedModule);
                    SequenceEditorModules.Add(sequenceEditorModule);
                }
            }
        }

        public override void DisconnectFromAds()
        {
            try
            {
                AdsTcClient.Dispose();
                ADS_WriteHelper.ClearHandles();
                Connected = false;
                foreach (var module in SequenceEditorModules)
                    module.Connected = false;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Failed to disconnect to the PLC.\n"
                                                    + e.Message);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)  // listen or test for event
                // raise event passing ourself in as event source and indicate name for property that has changed
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
