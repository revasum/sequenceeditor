﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Controls;

namespace SequencerInterface.Definitions
{
    public class DoubleRangeRule : ValidationRule
    {
        public double Min { get; set; }
        public double Max { get; set; }

        public DoubleRangeRule() { }

        public DoubleRangeRule(double min, double max)
        {
            Min = min; Max = max;
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double parameter = 0;

            try
            {
                if (((string)value).Length > 0)
                {
                    parameter = Double.Parse((String)value);
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Illegal characters or "
                                             + e.Message);
            }

            if (parameter == 0)
                return new ValidationResult(true, null);

            if ((parameter < this.Min) || (parameter > this.Max))
            {
                return new ValidationResult(false,
                    "Please enter a value within the range: "
                    + this.Min + " - " + this.Max);
            }
            return new ValidationResult(true, null);
        }
    }
}

