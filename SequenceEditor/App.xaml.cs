﻿using SequencerInterface.Definitions;
using SequencerInterface.Tools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SequenceEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string DefaultDirectory 
        {
            get 
            { 
                return @"C:\SequenceEditor"; 
            } 
        }

        public static DefaultSettings DefaultSettings 
        { 
            get; 
            set; 
        }


        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //checking for necessary dlls
            var fullFileNames = Directory.GetFiles(Directory.GetCurrentDirectory());

            List<String> fileNames = new List<string>();

            foreach (var file in fullFileNames)
            {
                fileNames.Add(System.IO.Path.GetFileName(file));
            }

            if(!fileNames.Contains("TwinCAT.Ads.dll"))
            {
                MessageBox.Show("Failure to load TwinCAT.Ads.dll \n Please ensure this file is in the same folder as SequenceEditor.exe \n Exiting", "Library File Missing", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }

            if (!fileNames.Contains("Xceed.Wpf.DataGrid.dll"))
            {
                MessageBox.Show("Failure to load Xceed.Wpf.DataGrid.dll \n Please ensure this file is in the same folder as SequenceEditor.exe \n Exiting", "Library File Missing", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }

            if (!fileNames.Contains("Xceed.Wpf.Toolkit.dll"))
            {
                MessageBox.Show("Failure to load Xceed.Wpf.Toolkit.dll \n Please ensure this file is in the same folder as SequenceEditor.exe \n Exiting", "Library File Missing", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }

            if (!fileNames.Contains("SequencerInterface.dll"))
            {
                MessageBox.Show("Failure to load SequencerInterface.dll \n Please ensure this file is in the same folder as SequenceEditor.exe \n Exiting", "Library File Missing", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }


            
            String resourceName =String.Empty;

            try
            {
                AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
                {
                    var assy = new AssemblyName(args.Name);

                    resourceName = $"AssemblyLoadingAndReflection.{assy.Name}.dll";

                    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                    {
                        byte[] assemblyData = stream != null ? new byte[stream.Length] : null;

                        stream?.Read(assemblyData, 0, assemblyData.Length);

                        return Assembly.Load(assemblyData);
                    }
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message} \n\n Library Name: {resourceName}", "Library Load Error", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
            
            //register event to hide focus when a user presses enter
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.KeyUpEvent, new System.Windows.Input.KeyEventHandler(TextBox_KeyUp));

            OpenDefaultSettings();
        }

        public static void OpenDefaultSettings()
        {
            DefaultSettings = XmlSerializerTools.Deserializer<DefaultSettings>(DefaultDirectory + @"\DefaultSettings.xml");
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            SequenceEditor.Properties.Settings.Default.Save();
        }

        /// <summary>
        ///     Move focus after user presses enter in a textbox (global)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                try
                {
                    ((UIElement)e.OriginalSource).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }
                catch { }
            }
        }
    }
}
